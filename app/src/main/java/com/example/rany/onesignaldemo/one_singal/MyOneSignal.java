package com.example.rany.onesignaldemo.one_singal;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.example.rany.onesignaldemo.DetailActivity;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class MyOneSignal extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new MyNotificationOpenHandler())
                .setNotificationReceivedHandler(new MyNotificationReceiveHandler())
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

    }
    class MyNotificationOpenHandler implements OneSignal.NotificationOpenedHandler {

        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            // get action from user
            OSNotificationAction.ActionType actionType = result.action.type;
            if (actionType == OSNotificationAction.ActionType.ActionTaken)
                Log.i("ooooo", "Button pressed with id: "
                        + result.action.actionID);

            // get additional data from notification
            JSONObject data = result.notification.payload.additionalData;
            String name, image;

            if (data != null) {
                name = data.optString("user", null);
                image = data.optString("image", null);
                if (name != null && image != null)
                    Log.i("OneSignalExample", "customkey set with value: "
                            + name+ " , "+ image);
                // parse data to detail screen
                 Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                 intent.putExtra("name", name);
                 intent.putExtra("image", image);
                 intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                 startActivity(intent);
            }
        }
    }

    class MyNotificationReceiveHandler implements OneSignal.NotificationReceivedHandler{

        @Override
        public void notificationReceived(OSNotification notification) {
            JSONObject data = notification.payload.additionalData;
            String name;

            if (data != null) {
                name = data.optString("name", null);
                Log.i("ooooo", "if Null value " + name);
                if (name != null)
                    Log.i("ooooo", "customkey set with value: " + name);
            }
        }
    }
}
