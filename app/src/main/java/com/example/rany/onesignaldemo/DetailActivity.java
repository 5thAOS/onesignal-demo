package com.example.rany.onesignaldemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class DetailActivity extends AppCompatActivity {

    private TextView result;
    private ImageView imvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        result = findViewById(R.id.tvResult);
        imvResult = findViewById(R.id.imvResult);
        Bundle bundle = getIntent().getExtras();
        result.setText(bundle.getString("name"));

        // Custom image with glide
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.override(700, 300);
        requestOptions.centerCrop();
        requestOptions.placeholder(R.drawable.ic_launcher_background);
        requestOptions.error(R.drawable.ic_launcher_foreground);

        Glide.with(this)
                .setDefaultRequestOptions(requestOptions)
                .load(bundle.getString("image"))
                .into(imvResult);

    }
}
