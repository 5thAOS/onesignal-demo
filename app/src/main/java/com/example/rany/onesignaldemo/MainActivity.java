package com.example.rany.onesignaldemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.onesignal.OneSignal;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.itm_entertainment:
                OneSignal.sendTag("Entertainment", "true");
                break;
            case R.id.itm_sport:
                OneSignal.sendTag("Sport", "true");
                break;
            case R.id.itm_subscribe:
                OneSignal.setSubscription(true);
                break;
            case R.id.itm_unsubscribe:
                OneSignal.setSubscription(false);
            case R.id.itm_clear:
                OneSignal.deleteTags(Arrays.asList("Entertainment", "Sport"));
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
